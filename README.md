# cassandra

Docker image for Apache Cassandra with a configurable hard limit on memory used by the JVM.

## How to run it as a service

```shell
docker service create \
  -p 7000:7000 -p 7001:7001 -p 7199:7199 -p 9042:9042 -p 9142:9142 \
  --hostname localhost \
  --name <service_name> \
  -e CASSANDRA_MAX_MEMORY=<max_memory_in_mb> \
  registry.gitlab.com/sadesyllas/cassandra
```
