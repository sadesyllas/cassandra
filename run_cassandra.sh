#!/bin/bash

if ! [ -f /etc/cassandra/cassandra-env.sh.backup ]; then
  cp /etc/cassandra/cassandra-env.sh /etc/cassandra/cassandra-env.sh.backup
fi

cp -f /etc/cassandra/cassandra-env.sh.backup /etc/cassandra/cassandra-env.sh

sed -i 's/8192/'${CASSANDRA_MAX_MEMORY:-8192}'/g' /etc/cassandra/cassandra-env.sh

hostname -b localhost

IP="$(ip address | grep inet | grep -v 127.0.0.1 | head -n 1 | awk '{gsub(/\/.+/, ""); print $2}')"

cp -f /etc/cassandra/cassandra.yaml.backup /etc/cassandra/cassandra.yaml

sed -E -i 's/-\.-\.-\.-/'$IP'/g' /etc/cassandra/cassandra.yaml

su -p -s /bin/bash -l -c '/usr/sbin/cassandra -f' cassandra
