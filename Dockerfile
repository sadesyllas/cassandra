FROM cassandra:latest

COPY run_cassandra.sh /run_cassandra

COPY cassandra.yaml /etc/cassandra/cassandra.yaml.backup

RUN chmod a+x /run_cassandra

RUN mkdir /home/cassandra && chown cassandra:cassandra /home/cassandra

CMD [ "/run_cassandra" ]
